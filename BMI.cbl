       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. PEERAYA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT   PIC 9(2)V9(2).
       01  HEIGHT   PIC 9(3)V9(2).
       01  BMI      PIC 9(2)V9(2).
       01  MSG_BMI  PIC X(50) VALUE SPACE.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Please input weight(kg.) : "
              WITH NO ADVANCING 
           ACCEPT WEIGHT
           DISPLAY "Please input height(cm.) : "
              WITH NO ADVANCING 
           ACCEPT HEIGHT 
           DISPLAY "-----------------------------------"

           COMPUTE HEIGHT = HEIGHT / 100
           COMPUTE BMI = WEIGHT / (HEIGHT * HEIGHT )

           DISPLAY "BMI = " BMI " kg/m^2"

           EVALUATE BMI  
              WHEN 0 THRU 18.50 MOVE "Underweight / Thin" TO MSG_BMI 
              WHEN 18.50 THRU 22.90 MOVE "Normal (Healthy)" TO MSG_BMI 
              WHEN 23 THRU 24.90 MOVE "Overweight / Obese Class I" TO 
                 MSG_BMI 
              WHEN 25 THRU 29.90 MOVE "Obese / Obese Class II" TO 
                 MSG_BMI 
              WHEN OTHER MOVE "Extremely obese / Obese Class III" TO 
                 MSG_BMI 

           END-EVALUATE
           DISPLAY "You are " MSG_BMI 
           .
